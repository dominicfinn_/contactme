Contact me, Referral site
=================

This is the development test we have used for the past year or so at North 51. 
I thought I would put it out there for anyone to fork and take a look. 

If you are a potential candidate please fork the project and send a link to your clone when you are ready for us to take a look.

The project is based on asp.net mvc. There is no persistence technology chosen. This is intentional as we have found that when we used
NHibernate people tended to get caught up in the details of that. 

Under the docs folder is the instructions of what we are asking for. There is very little work to do but the devil is in the detail.

Please remember to read the spec. Print it off, cross out the lines if you felt you have met the requirements. If you have any questions,
just ask. If you don't agree with something, just ask. 

There are a couple of essential things:

- Read the spec
- Make sure the project builds
- Ensure a submitted self-referral is persisted in some way and returned on the report screen
- Watch out for the requirements surrounding the contact dates

Things we are looking out for: 

- Proficiency in asp.net mvc
- Ability to pick details up in specifications
- Use of unit testing
- Understanding of SOLID principles

If you notice anything odd or any mistakes just let me know!

Thanks!