using System;
using System.Collections.Generic;
using ContactMe.Core.Domain;
using ContactMe.Core.Repository;

namespace ContactMe.Core.Services
{
    public class ContactService : IContactService
    {
        private readonly IContactRepository contactRepository;

        public ContactService(IContactRepository contactRepository)
        {
            this.contactRepository = contactRepository;
        }

        public IEnumerable<Contact> ContactsByFirstContactDate()
        {
            throw new NotImplementedException();
        }

        public void SaveContact(Contact c)
        {
            throw new NotImplementedException();
        }
    }
}